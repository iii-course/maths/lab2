import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputFormComponent } from './components/forms/input-form/input-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from '@angular/material/button';
import { CalculatorComponent } from './components/calculator/calculator.component';
import {MatTableModule} from "@angular/material/table";
import { NumericVectorsFormattingPipe } from './core/pipes/numeric-vectors-formatting/numeric-vectors-formatting.pipe';
import {CommonModule, DecimalPipe} from "@angular/common";
import { StringFormattingPipe } from './core/pipes/string-formatting/string-formatting.pipe';
import { NumberFormattingPipe } from './core/pipes/number-formatting/number-formatting.pipe';

@NgModule({
  declarations: [
    AppComponent,
    InputFormComponent,
    CalculatorComponent,
    NumericVectorsFormattingPipe,
    StringFormattingPipe,
    NumberFormattingPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatTableModule,
    CommonModule
  ],
  providers: [DecimalPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
