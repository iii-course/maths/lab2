import {Population} from "./population/population.model";

export enum Stages {
  INITIAL = 'INITIAL',
  SELECTION = 'SELECTION',
  CROSSOVER = 'CROSSOVER',
  MUTATION = 'MUTATION',
}

export interface Iteration {
  numberOfIteration: number,
  population: Population,
  stage: Stages
}

export interface Output {
  population: Population,
  functionValue: number,
  finalPoint: number | number[],
}
