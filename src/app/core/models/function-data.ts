import { FunctionOfNumericVector} from "./functions.model";

export interface FunctionData {
  plainTextFormula: string,
  func: FunctionOfNumericVector,
  numberOfArguments: number
}
