import {NumericVector} from "../vectors/vector.model";
import {FunctionOfNumericVector} from "../functions.model";

export class FitnessFunction {
  constructor(
    private func: FunctionOfNumericVector,
    private isMinimization: boolean
  ) {}

  public howFitIs(vector: NumericVector): number {
    const funcResult = this.func(...vector.coordinates);
    return this.isMinimization ? - funcResult : funcResult;
  }
}
