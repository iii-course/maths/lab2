import {Population} from "../population/population.model";
import {FitnessFunction} from "../fitness-function/fitness-function";

export interface BreedingSelector {
  select: (population: Population, fitnessFunction: FitnessFunction, isMinimization: boolean) => Population
}
