import {NumericVector} from "../vectors/vector.model";

export interface ValueWithProbability {
  value: NumericVector,
  probability: number
}

export class RouletteWheel {
  private sorted = false;
  private readonly minProbability = 0;

  constructor(private values: ValueWithProbability[]) {
    this.normalizeProbabilities();
  }

  public getNextValue(): NumericVector {
    let bottomBoundary = 0;
    let topBoundary = 0;
    const random = Math.random();
    for (let value of this.values) {
      topBoundary += value.probability;
      if (bottomBoundary < random && random < topBoundary) {
        return value.value;
      }
      bottomBoundary += value.probability;
    }
    const defaultValue = new NumericVector([0]);

    return this.values[this.values.length - 1].value || defaultValue;
  }

  private normalizeProbabilities(): void {
    this.sortValuesByProbabilities();

    this.mapProbabilitiesToPercents();

    const sumOfProbabilities = this.getSumOfProbabilities();

    this.values = this.values.map(value => {
      const normalizedProbability = value.probability / sumOfProbabilities;
      return {value: value.value, probability: normalizedProbability}
    });
  }

  private mapProbabilitiesToPercents(): void {
    const lowestFit = this.getLowestFit();
    const highestFit = this.getHighestFit();

    this.values = this.values.map(value => {
      const mappedProbability = this.mapProbabilityToPercents(value.probability, lowestFit, highestFit);
      return {value: value.value, probability: mappedProbability}
    });
  }

  private getLowestFit(): number {
    this.sortValuesByProbabilities();
    return this.values[0].probability;
  }

  private getHighestFit(): number {
    this.sortValuesByProbabilities();
    return this.values[this.values.length - 1].probability;
  }

  private sortValuesByProbabilities(): void {
    if (!this.sorted) {
      this.values.sort(sortByProbabilitiesAsc);
    }
    this.sorted = true;
  }

  private mapProbabilityToPercents(probability: number, lowestFit: number, highestFit: number): number {
    return this.minProbability + Math.abs(probability - lowestFit) / Math.abs(highestFit - lowestFit);
  }

  private getSumOfProbabilities(): number {
    return this.values.reduce((sum, cur) => sum + cur.probability, 0);
  }
}

function sortByProbabilitiesAsc(value1: ValueWithProbability, value2: ValueWithProbability) {
  return value1.probability - value2.probability;
}

function sortByProbabilitiesDesc(value1: ValueWithProbability, value2: ValueWithProbability) {
  return value2.probability - value1.probability;
}
