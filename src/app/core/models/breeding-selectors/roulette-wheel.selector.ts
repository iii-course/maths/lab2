import {BreedingSelector} from "./breeding-selector.model";
import {Population, VectorsGroup} from "../population/population.model";
import {RouletteWheel, ValueWithProbability} from "./roulette-wheel";
import {FitnessFunction} from "../fitness-function/fitness-function";

export class RouletteWheelSelector implements BreedingSelector {
  public select(population: Population, fitnessFunction: FitnessFunction, isMinimization: boolean): Population {
    const probabilities: ValueWithProbability[] = population.group.map(individual => ({
      value: individual,
      probability: fitnessFunction.howFitIs(individual)
    }));

    const roulette = new RouletteWheel(probabilities);

    const newGroup: VectorsGroup = [];

    for (let i = 0; i < population.group.length; i += 1) {
      const nextIndividual = roulette.getNextValue();
      newGroup.push(nextIndividual);
    }

    return new Population(newGroup);
  }
}
