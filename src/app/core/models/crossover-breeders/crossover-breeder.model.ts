import {BinaryGroup, Population, VectorsGroup} from "../population/population.model";
import {NumericVector} from "../vectors/vector.model";
import {Pairer} from "./pairer";

export type Pair = NumericVector[] | string[];

export class CrossoverBreeder {
  public crossbreed(parentsPopulation: Population): Population {
    const pairs = Pairer.formPairs(parentsPopulation);

    let childrenGroup: VectorsGroup | BinaryGroup = [];

    for (let parents of pairs) {
      const children = this.getChildren(parents);
      childrenGroup = [...childrenGroup, ...children] as VectorsGroup | BinaryGroup;
    }

    return new Population(childrenGroup);
  }

  public getChildren(parents: Pair): Pair {
    return parents;
  }
}
