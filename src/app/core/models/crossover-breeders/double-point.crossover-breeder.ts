import {CrossoverBreeder, Pair} from "./crossover-breeder.model";
import {BinaryGroup, VectorsGroup} from "../population/population.model";
import {isBinary} from "../../helpers/population.helpers";
import {getRandomInteger} from "../../helpers/number.helpers";
import {NumericVector} from "../vectors/vector.model";
import {NumericCoordinates} from "../vectors/coordinates.model";

export class DoublePointCrossoverBreeder extends CrossoverBreeder {
  public getChildren(parents: Pair): Pair {
    return isBinary(parents) ? this.crossbreedBinaryStrings(parents) : this.crossbreedVectors(parents);
  }

  private crossbreedBinaryStrings(parents: BinaryGroup): Pair {
    const [parent1, parent2] = parents;

    if (!this.canBreedBinaryStrings(parent1, parent2)) {
      return parents;
    }

    const splitParent1 = parent1.split('');
    const splitParent2 = parent2.split('');

    const [child1, child2] = this.crossbreedParents(splitParent1, splitParent2);

    return [child1.join(''), child2.join('')];
  }

  private crossbreedVectors(parents: VectorsGroup): Pair {
    const [parent1, parent2] = parents;

    if (!this.canBreedVectors(parent1, parent2)) {
      return parents;
    }

    const parent1Coordinates = parent1.coordinates;
    const parent2Coordinates = parent2.coordinates;

    const [
      child1Coordinates,
      child2Coordinates
    ] = this.crossbreedParents(parent1Coordinates, parent2Coordinates);

    return [
      new NumericVector(child1Coordinates as NumericCoordinates),
      new NumericVector(child2Coordinates as NumericCoordinates)
    ];
  }

  private crossbreedParents(
    parent1: NumericCoordinates | string[],
    parent2: NumericCoordinates | string[]
  ): NumericCoordinates[] | string[][] {
    let child1 = [...parent1];
    let child2 = [...parent2];

    const {length} = parent1;

    const begin = getRandomInteger(length);
    const end = getRandomInteger(length);

    for (let i = begin; i < length && i < end; i += 1) {
      this.crossbreedAt(
        i, [parent1, parent2], [child1, child2]
      );
    }

    for (let i = 0; i < end && end < begin; i ++) {
      this.crossbreedAt(
        i, [parent1, parent2], [child1, child2]
      );
    }

    return [child1, child2] as NumericCoordinates[] | string[][];
  }

  private crossbreedAt(i: number, parents: any[][], children: any[][]) {
    children[0][i] = parents[1][i];
    children[1][i] = parents[0][i];
  }

  private canBreedVectors(parent1: NumericVector | undefined, parent2: NumericVector | undefined): boolean {
    return !!(parent1 && parent2 && parent1.coordinates.length === parent2.coordinates.length);
  }

  private canBreedBinaryStrings(parent1: string | undefined, parent2: string | undefined): boolean {
    return !!(parent1 && parent2 && parent1.length === parent2.length);
  }
}
