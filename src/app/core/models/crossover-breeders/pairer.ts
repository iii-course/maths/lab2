import {BinaryGroup, Population, VectorsGroup} from "../population/population.model";
import { getRandomInteger} from "../../helpers/number.helpers";
import {Pair} from "./crossover-breeder.model";

export class Pairer {
  public static formPairs(population: Population): Pair[] {
    const group = population.hasBinaryFormat() ? population.binaryGroup : population.group;

    const totalNumberOfPairs = Math.floor(group.length / 2);
    const pairs: Pair[] = [];

    while (pairs.length < totalNumberOfPairs) {
      const pair = this.getNextPair(group);
      pairs.push(pair);
    }

    return pairs;
  }

  private static getNextPair(group: VectorsGroup | BinaryGroup): Pair {
    const maxIndex = group.length - 1;

    const parent1Index = getRandomInteger(maxIndex);
    let parent2Index = parent1Index;

    while (parent1Index === parent2Index) {
      parent2Index = getRandomInteger(maxIndex);
    }

    return [group[parent1Index], group[parent2Index]] as Pair;
  }

  private static isPairAlreadyPresent(pairs: Pair[], pairToCheck: Pair): boolean {
    return !!pairs.find(pair => {
      return pair[0] === pairToCheck[0] && pair[1] === pairToCheck[1] ||
        pair[0] === pairToCheck[1] && pair[1] === pairToCheck[0]
    });
  }
}
