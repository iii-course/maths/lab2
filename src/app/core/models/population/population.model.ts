import {NumericVector} from "../vectors/vector.model";
import { getBinaryGroup, getRealGroup, isBinary, isReal} from "../../helpers/population.helpers";
import {FitnessFunction} from "../fitness-function/fitness-function";

export type BinaryGroup = string[];
export type VectorsGroup = NumericVector[];

interface IndividualWithFitnessValue {
  individual: NumericVector,
  fitnessValue: number
}

export class Population {
  private individuals!: VectorsGroup;
  private binaryIndividuals!: BinaryGroup;

  constructor(group: VectorsGroup | BinaryGroup) {
    if (isReal(group)) {
      this.individuals = group;
    } else if (isBinary(group)) {
      this.binaryIndividuals = group;
    }
  }

  public get group(): VectorsGroup {
    if (!this.individuals) {
      this.setGroup();
    }
    return this.individuals;
  }

  public get binaryGroup(): BinaryGroup {
    if (!this.binaryIndividuals) {
      this.setBinaryGroup();
    }
    return this.binaryIndividuals;
  }

  public get size(): number {
    return this.group.length || this.binaryGroup.length;
  }

  public uniteWith(anotherPopulation: Population): Population {
    return new Population(this.group.concat(...anotherPopulation.group));
  }

  public sortByFitnessAsc(fitnessFunction: FitnessFunction): void {
    this.individuals.sort((a, b) => {
      return fitnessFunction.howFitIs(a) - fitnessFunction.howFitIs(b)
    });
  }

  public sortByFitnessDesc(fitnessFunction: FitnessFunction): void {
    this.individuals.sort((a, b) => {
      return fitnessFunction.howFitIs(b) - fitnessFunction.howFitIs(a)
    });
  }

  public getFittestIndividual(fitnessFunction: FitnessFunction): NumericVector {
    this.sortByFitnessAsc(fitnessFunction);
    return this.individuals[this.individuals.length - 1];
  }

  public hasConverged(precision: number, fitnessFunction: FitnessFunction): boolean {
    const norms = this.group.map(vector => vector.norm);
    norms.sort();

    const fitnessValues = this.group.map(vector => fitnessFunction.howFitIs(vector));
    fitnessValues.sort();

    return this.getDelta(norms[norms.length - 1], norms[0]) <= precision &&
      this.getDelta(fitnessValues[fitnessValues.length - 1], fitnessValues[0]) <= precision;
  }

  public hasBinaryFormat(): boolean {
    return this.group.length > 0 && this.group[0].length === 1;
  }

  private setGroup(): void {
      this.individuals = getRealGroup(this.binaryIndividuals);
  }

  private setBinaryGroup(): void {
    this.binaryIndividuals = getBinaryGroup(this.individuals);
  }

  private getDelta(n1: number, n2: number): number {
    return Math.abs(n1 - n2);
  }
}

function sortByFitnessAsc(i1: IndividualWithFitnessValue, i2: IndividualWithFitnessValue): number {
  return i1.fitnessValue - i2.fitnessValue;
}
