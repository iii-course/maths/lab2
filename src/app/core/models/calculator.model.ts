import {Iteration, Output} from "./output.model";
import {AlgorithmParams, InputValues} from "./input.model";
import {Observable} from "rxjs";

export interface Calculator {
  iterations$: Observable<Iteration[]>,
  calculate: (inputValues: InputValues, algorithmParams: AlgorithmParams) => Output | Error
}
