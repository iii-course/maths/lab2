import {NumericCoordinates} from "./vectors/coordinates.model";

export type FunctionOfNumericVector = (...args: NumericCoordinates) => number;

