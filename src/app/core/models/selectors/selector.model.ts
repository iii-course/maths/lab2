import {Population} from "../population/population.model";
import {FunctionOfNumericVector} from "../functions.model";
import {FitnessFunction} from "../fitness-function/fitness-function";

export interface Selector {
  select(
    parents: Population,
    children: Population,
    func: FunctionOfNumericVector,
    fitnessFunction: FitnessFunction
  ): Population;
}
