import {Selector} from "./selector.model";
import {Population, VectorsGroup} from "../population/population.model";
import {FunctionOfNumericVector} from "../functions.model";
import {FitnessFunction} from "../fitness-function/fitness-function";

export class EliteSelector implements Selector {
  public select(
    parents: Population,
    children: Population,
    func: FunctionOfNumericVector,
    fitnessFunction: FitnessFunction
  ): Population {
    const resultPopulationSize = parents.size;
    const resultGroup: VectorsGroup = [];

    const unitedPopulation = parents.uniteWith(children);

    unitedPopulation.sortByFitnessDesc(fitnessFunction);

    const unitedGroup = [...unitedPopulation.group];

    for (let i = 0; i < resultPopulationSize; i += 1) {
      resultGroup.push(unitedGroup[i]);
    }

    return new Population(resultGroup);
  }
}
