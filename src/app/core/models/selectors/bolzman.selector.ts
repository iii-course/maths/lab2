import {Selector} from "./selector.model";
import {Population, VectorsGroup} from "../population/population.model";
import {NumericVector} from "../vectors/vector.model";
import {FunctionOfNumericVector} from "../functions.model";
import {getRandomInteger} from "../../helpers/number.helpers";
import {FitnessFunction} from "../fitness-function/fitness-function";

export class BolzmanSelector implements Selector {
  private temperature = 10;

  public select(parents: Population, children: Population, func: FunctionOfNumericVector, _: FitnessFunction): Population {
    const resultPopulationSize = parents.size;
    const resultGroup: VectorsGroup = [];

    const unitedPopulation = parents.uniteWith(children);

    while (resultGroup.length < resultPopulationSize) {
      const [individual1, individual2] = this.chooseTwoRandomIndividuals(unitedPopulation);
      const chosenIndividual = this.chooseBetweenIndividuals(individual1, individual2, func);
      resultGroup.push(chosenIndividual);
    }

    return new Population(resultGroup);
  }

  private chooseBetweenIndividuals(
    individual1: NumericVector, individual2: NumericVector, func: FunctionOfNumericVector
  ): NumericVector {
    const expPow = (func(...individual1.coordinates) - func(...individual2.coordinates)) / this.temperature;
    const p = 1 / (1 + Math.pow(Math.E, expPow));
    return p > Math.random() ? individual1 : individual2;
  }

  private chooseTwoRandomIndividuals(population: Population): NumericVector[] {
    const maxIndex = population.size - 1;
    const random1Index = getRandomInteger(maxIndex);
    let random2Index = getRandomInteger(maxIndex);

    while (random2Index === random1Index) {
      random2Index = getRandomInteger(maxIndex);
    }

    return [
      population.group[random1Index],
      population.group[random2Index]
    ];
  }
}
