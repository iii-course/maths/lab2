import {Interval} from "../vectors/interval.model";
import {Population} from "../population/population.model";

export interface PopulationPrerequisites {
  populationSize: number,
  interval?: Interval,
  numberOfGenesInChromosome?: number
}

export interface PopulationGenerator {
  generatePopulation: (populationPrerequisites: PopulationPrerequisites) => Population
}
