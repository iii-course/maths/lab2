import {PopulationGenerator, PopulationPrerequisites} from "./population-generator.model";
import {Interval} from "../vectors/interval.model";
import {Population} from "../population/population.model";
import {generateUniqueVectors} from "../../helpers/number.helpers";

export class RandomPopulationGenerator implements PopulationGenerator {
  private readonly DEFAULT_INTERVAL = new Interval({begin: 0, end: 100});

  public generatePopulation(populationPrerequisites: PopulationPrerequisites): Population {
    const {populationSize, numberOfGenesInChromosome, interval} = populationPrerequisites;

    const group = generateUniqueVectors(
      populationSize,
      numberOfGenesInChromosome || 1,
      interval || this.DEFAULT_INTERVAL
    );

    return new Population(group);
  }
}
