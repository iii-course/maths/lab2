import {Interval} from "./vectors/interval.model";
import {FunctionOfNumericVector} from "./functions.model";
import {PopulationGenerator} from "./population-generators/population-generator.model";
import {BreedingSelector} from "./breeding-selectors/breeding-selector.model";
import {CrossoverBreeder} from "./crossover-breeders/crossover-breeder.model";
import {Mutator} from "./mutators/mutator.model";
import {Selector} from "./selectors/selector.model";
import {FunctionData} from "./function-data";

export interface InputValues {
  populationSize: number,
  maxNumberOfIterations: number,
  precision: number,
  mutationProbability: number,
  interval?: Interval,
  functionData: FunctionData
}

export interface AlgorithmParams {
  populationGenerator: PopulationGenerator,
  breedingSelector: BreedingSelector,
  crossoverBreeder: CrossoverBreeder,
  mutator: Mutator,
  selector: Selector
}
