type IntervalCoordinates = {
  begin: number,
  end: number
};

export class Interval {
  begin!: number;
  end!: number;

  constructor({begin, end}: IntervalCoordinates) {
    this.begin = begin;
    this.end = end;
  }

  public get length(): number {
    return Math.abs(this.end - this.begin);
  }
}
