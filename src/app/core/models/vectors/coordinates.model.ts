export type NumericCoordinates = number[];

export interface NamedCoordinates {
  [argumentName: string]: number
}
