import {Mutator} from "./mutator.model";
import {NumericVector} from "../vectors/vector.model";
import {getRandomInteger, getTrue} from "../../helpers/number.helpers";

export class MultiPointMutator extends Mutator {
  public mutateBinaryChromosome(chromosome: string): string {
    const numberOfMutations = getRandomInteger(chromosome.length - 1);
    const alreadyMutatedIndexes = new Set<number>();

    const genes = chromosome.split('');

    while (alreadyMutatedIndexes.size < numberOfMutations) {
      const mutationIndex = getRandomInteger(chromosome.length - 1);

      const shouldMutate = !alreadyMutatedIndexes.has(mutationIndex) &&
        this.canBinaryChromosomeMutateAt(mutationIndex, genes);

      if (shouldMutate) {
        alreadyMutatedIndexes.add(mutationIndex);
        genes[mutationIndex] = getTrue(0.5) ? '0' : '1';
      }
    }

    return genes.join('');
  }

  public mutateVectorChromosome(chromosome: NumericVector): NumericVector {
    const genes = [...chromosome.coordinates];

    const numberOfMutations = getRandomInteger(genes.length);
    const alreadyMutatedIndexes = new Set<number>();

    while (alreadyMutatedIndexes.size < numberOfMutations) {
      const mutationIndex = getRandomInteger(chromosome.length - 1);
      if (!alreadyMutatedIndexes.has(mutationIndex)) {
        alreadyMutatedIndexes.add(mutationIndex);
        genes[mutationIndex] = this.calculateMutatedNumber(genes[mutationIndex]);
      }
    }

    return new NumericVector(genes);
  }

  private calculateMutatedNumber(oldNumber: number, intervalOfChange: number = 10, m: number = 20): number {
    const factor = getTrue(0.5) ? 1 : -1;

    let sum = 0;
    for (let i = 1; i <= m; i += 1) {
      const a = getTrue(1 / m) ? 1 : 0;
      sum += a * Math.pow(2, -i);
    }

    return oldNumber + factor * sum;
  }

  private canBinaryChromosomeMutateAt(i: number, chromosome: string[]): boolean {
    return chromosome[i] === '0' || chromosome[i] === '1';
  }
}
