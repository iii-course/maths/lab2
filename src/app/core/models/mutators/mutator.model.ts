import {BinaryGroup, Population, VectorsGroup} from "../population/population.model";
import {NumericVector} from "../vectors/vector.model";

export class Mutator {
  public mutate(population: Population, mutationProbability: number): Population {
    const group = population.hasBinaryFormat() ? population.binaryGroup : population.group;
    const mutatedGroup: BinaryGroup | VectorsGroup = [];

    for (let individual of group) {
      const mutatedIndividual = this.mutateChromosome(individual, mutationProbability);
      mutatedGroup.push(mutatedIndividual as any);
    }

    return new Population(mutatedGroup);
  }

  public mutateChromosome(chromosome: string | NumericVector, mutationProbability: number): string | NumericVector {
    const shouldMutate = Math.random() < mutationProbability;
    if (chromosome instanceof NumericVector && shouldMutate) {
      return this.mutateVectorChromosome(chromosome);
    }
    if (shouldMutate) {
      return this.mutateBinaryChromosome(chromosome as string);
    }
    return chromosome;
  }

  public mutateBinaryChromosome(chromosome: string): string {
    return chromosome;
  }

  public mutateVectorChromosome(chromosome: NumericVector): NumericVector {
    return chromosome;
  }
}
