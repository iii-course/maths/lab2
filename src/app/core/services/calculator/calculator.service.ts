import {Injectable} from '@angular/core';
import {Calculator} from "../../models/calculator.model";
import {Iteration, Output, Stages} from "../../models/output.model";
import {AlgorithmParams, InputValues} from "../../models/input.model";
import {Population, VectorsGroup} from "../../models/population/population.model";
import {FitnessFunction} from "../../models/fitness-function/fitness-function";
import {BehaviorSubject} from "rxjs";
import {NumericVector} from "../../models/vectors/vector.model";
import {NumericCoordinates} from "../../models/vectors/coordinates.model";

@Injectable({
  providedIn: 'root'
})
export class CalculatorService implements Calculator {
  private isMinimization = true;
  private iterationsSubject = new BehaviorSubject<Iteration[]>([]);

  public iterations$ = this.iterationsSubject.asObservable();

  constructor() { }

  public calculate(inputValues: InputValues, algorithmParams: AlgorithmParams): Output | Error {
    /*
    START
    Generate the initial population
    Compute fitness
    REPEAT
        Selection
        Crossover
        Mutation
        Compute fitness
    UNTIL population has converged
    STOP
     */
    this.resetIterations();

    const {
      populationSize, maxNumberOfIterations, precision, mutationProbability, interval, functionData
    } = inputValues;
    const {func} = functionData;
    const {
      populationGenerator: generator, breedingSelector, crossoverBreeder, mutator, selector
    } = algorithmParams;

    const fitnessFunction = new FitnessFunction(func, true);

    let population = generator.generatePopulation({
      populationSize, interval, numberOfGenesInChromosome: functionData.numberOfArguments
    });
    this.pushIteration(0, Stages.INITIAL, population);

    let i = 0;
    while (i < maxNumberOfIterations) { // && !population.hasConverged(precision, fitnessFunction)
      const parents = breedingSelector.select(population, fitnessFunction, this.isMinimization);
      this.pushIteration(i, Stages.SELECTION, parents);

      const children = crossoverBreeder.crossbreed(parents);
      this.pushIteration(i, Stages.CROSSOVER, children);

      const mutatedChildren = mutator.mutate(children, mutationProbability);
      this.pushIteration(i, Stages.MUTATION, mutatedChildren);

      population = selector.select(parents, mutatedChildren, func, fitnessFunction);
      this.pushIteration(i, Stages.SELECTION, population);

      i += 1;
    }

    const finalPoint = population.getFittestIndividual(fitnessFunction).coordinates;
    const functionValue = func(...finalPoint);

    return {finalPoint, population, functionValue};
  }

  private resetIterations(): void {
    this.iterationsSubject.next([]);
  }

  private pushIteration(numberOfIteration: number, stage: Stages, population: Population) {
    const group: VectorsGroup = [...population.group];
    this.iterationsSubject.next([
      ...this.iterationsSubject.value, {numberOfIteration, stage, population: new Population(group)}
    ]);
  }
}
