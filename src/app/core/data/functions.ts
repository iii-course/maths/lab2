import {FunctionData} from "../models/function-data";

const functionsOfOneArgument: FunctionData[] = [
  {
    plainTextFormula: 'x^2 + 4e^(-x/4)',
    func: (x: number) => {
      return Math.pow(x, 2) +
        4 * Math.pow(Math.E, -x / 4);
    },
    numberOfArguments: 1,
  },
  {
    plainTextFormula: 'x^3 - 3x^2 - 4',
    func: (x: number) => {
      return Math.pow(x, 3) -
        3 * Math.pow(x, 2) - 4;
    },
    numberOfArguments: 1,
  }
]

const functionsOfManyArguments: FunctionData[] = [
  {
    plainTextFormula: 'x^2 + 7*z^2 - 4*x*z - 3*y - 2*z',
    func: (x: number, y: number, z: number) => {
      return Math.pow(x, 2) +
        7 * Math.pow(z, 2) -
        4 * x * z -
        3 * y -
        2 * z;
    },
    numberOfArguments: 3,
  },
  {
    plainTextFormula: '2*x^2 + x*y + y^2',
    func: (x: number, y: number) => {
      return 2 * Math.pow(x, 2) + x * y + Math.pow(y, 2);
    },
    numberOfArguments: 2,
  },
  {
    plainTextFormula: 'x^2 + x*y + y^2 - 3*x - 6*y',
    func: (x: number, y: number) => {
      return Math.pow(x, 2) + x * y + Math.pow(y, 2) - 3 * x - 6 * y;
    },
    numberOfArguments: 2,
  },
  {
    plainTextFormula: 'x^2 - x*y + y^2 + 3*x - 2*y + 1',
    func: (x: number, y: number) => {
      return Math.pow(x, 2) - x * y + Math.pow(y, 2) + 3 * x - 2 * y + 1;
    },
    numberOfArguments: 2,
  },
  {
    plainTextFormula: 'x^3 + y^3 - 3*x*y',
    func: (x: number, y: number) => {
      return Math.pow(x, 3) + Math.pow(y, 3) - 3 * x * y;
    },
    numberOfArguments: 2,
  },
  {
    plainTextFormula: '(x - 2)^2 + 3 * (y - 1)^2',
    func: (x: number, y: number) => {
      return Math.pow(x - 2, 2) + 3 * Math.pow(y - 1, 2);
    },
    numberOfArguments: 2,
  }
];

export const functions: FunctionData[] = [
  ...functionsOfOneArgument,
  ...functionsOfManyArguments
];
