import {NumericVector} from "../models/vectors/vector.model";
import {BinaryGroup, VectorsGroup} from "../models/population/population.model";
import {getBinaryFittedToLength, getMaxBinaryLength} from "./binary.helpers";

export function isBinary(x: string[] | number[] | NumericVector[]): x is BinaryGroup {
  return x.length > 0 && typeof x[0] === 'string';
}

export function isReal(x: string[] | NumericVector[]): x is VectorsGroup {
  return x.length > 0 && x[0] instanceof NumericVector;
}

export function canBeConvertedToBinary(x: VectorsGroup | undefined): boolean {
  return !!x && x.length > 0 && x[0].coordinates.length === 1;
}

export function getBinaryGroup(realGroup: VectorsGroup | undefined): BinaryGroup {
  if (realGroup && canBeConvertedToBinary(realGroup)) {
    const flattened: number[] = realGroup.map(individual => individual.coordinates).flat();
    const binaryLength = getMaxBinaryLength(flattened);
    return flattened.map(individual => {
      const binaryString = individual.toString(2);
      return getBinaryFittedToLength(binaryString, binaryLength);
    });
  }
  return [];
}

export function getRealGroup(binaryGroup: BinaryGroup | undefined): VectorsGroup {
  if (binaryGroup) {
    return binaryGroup.map(binaryIndividual => new NumericVector([
      toDecimal(binaryIndividual)
    ]));
  }
  return [];
}

function toDecimal(binary: string): number {
  const [integer, decimal] = binary.split('.');
  const integerDecimal = Number.parseInt(integer, 2);
  const decimalDecimal = Number.parseInt(decimal, 2) / Math.pow(2, decimal.length);
  return integerDecimal + decimalDecimal;
}
