import {Interval} from "../models/vectors/interval.model";
import {NumericVector} from "../models/vectors/vector.model";

export function getNumberInRange(interval: Interval): number {
  const min = interval.begin;
  const max = interval.end;
  return Math.random() * (max - min + 1) + min;
}

export function getRandomInteger(max: number): number {
  return Math.round(Math.random() * max);
}

export function generateUniqueVectors(count: number, tupleSize: number, interval: Interval): NumericVector[] {
  const set = new Set<number[]>();
  while (set.size < count) {
    const nextTuple = [];
    while (nextTuple.length < tupleSize) {
      nextTuple.push(getNumberInRange(interval));
    }
    if (!set.has(nextTuple)) {
      set.add(nextTuple);
    }
  }
  return Array.from(set).map(coordinates => new NumericVector(coordinates));
}

export function getTrue(probability: number): boolean {
  return Math.random() < probability;
}
