export function getLengthOfLongestString(array: string[]): number {
  return array.reduce((a, b) =>  a.length > b.length ? a : b).length;
}

export function getMaxNumber(array: number[]) {
  return Math.max(...array);
}

export function getMinNumber(array: number[]) {
  return Math.min(...array);
}

export function isNumbersArray(x: any): x is number[] {
  return Array.isArray(x) && x.length > 0 && typeof x[0] === 'number';
}

export function isArrayOfNumberTuples(x: any): x is number[][] {
  return Array.isArray(x) && x.length > 0 && isNumbersArray(x[0]);
}

export function isStringsArray(x: any): x is string[] {
  return Array.isArray(x) && x.length > 0 && typeof x[0] === 'string';
}

export function isArrayOfStringsTuples(x: any): x is string[][] {
  return Array.isArray(x) && x.length > 0 && isStringsArray(x[0]);
}
