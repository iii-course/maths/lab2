import {getMaxNumber, getMinNumber} from "./arrays.helpers";

function getIntegerPartFittedToLength(binary: string, length: number): string {
  if (binary.length > length) {
    return binary.substring(length)
  } else if (binary.length === length) {
    return binary;
  }
  // binary.length < length
  let zeros: string[] = Array(length).fill('0');
  let binaryArray: string[] = binary.split('');
  const startIndex = length - binary.length;
  for (let i = startIndex; i < length; i += 1) {
    zeros[i] = binaryArray[i - startIndex];
  }
  return zeros.join('');
}

function getDecimalPartFittedToLength(binary: string, length: number): string {
  if (binary.length > length) {
    return binary.substring(0, length)
  } else if (binary.length === length) {
    return binary;
  }
  // binary.length < length
  let zeros = Array(length).fill('0');
  let binaryArray = binary.split('');
  for (let i = 0; i < binary.length; i += 1) {
    zeros[i] = binaryArray[i];
  }
  return zeros.join('');
}

export function getBinaryFittedToLength(binary: string, integerPartLength: number, decimalPartLength: number = 3): string {
  const [integerPart, decimalPart] = binary.split('.');
  const integerPartNormalized = getIntegerPartFittedToLength(integerPart || '', integerPartLength);
  const decimalPartNormalized = getDecimalPartFittedToLength(decimalPart || '', decimalPartLength);
  return `${integerPartNormalized}.${decimalPartNormalized}`;
}

export function getMaxBinaryLength(array: number[]) {
  const maxNumber = getMaxNumber(array);
  return Math.ceil(Math.sqrt(maxNumber));
}
