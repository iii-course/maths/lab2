import { Pipe, PipeTransform } from '@angular/core';
import {DecimalPipe} from "@angular/common";
import {NumericVector} from "../../models/vectors/vector.model";


@Pipe({
  name: 'numericVectorsFormatting'
})
export class NumericVectorsFormattingPipe implements PipeTransform {

  constructor(private decimalPipe: DecimalPipe) {}

  transform(vectors: NumericVector[], decimalFormat?: string): string[][] {
    return vectors.map(vector => this.transformArray(vector.coordinates, decimalFormat));
  }

  private transformArray(numbers: number[], decimalFormat?: string): string[] {
    return numbers
      .map(num => this.decimalPipe.transform(num, decimalFormat))
      .filter(str => !!str) as string[];
  }
}
