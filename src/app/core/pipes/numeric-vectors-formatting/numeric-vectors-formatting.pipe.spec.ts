import { NumericVectorsFormattingPipe } from './numeric-vectors-formatting.pipe';
import {DecimalPipe} from "@angular/common";

describe('NumberFormattingPipe', () => {
  it('create an instance', () => {
    const pipe = new NumericVectorsFormattingPipe({} as DecimalPipe);
    expect(pipe).toBeTruthy();
  });
});
