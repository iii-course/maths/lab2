import { Pipe, PipeTransform } from '@angular/core';
import {DecimalPipe} from "@angular/common";

@Pipe({
  name: 'numberFormatting'
})
export class NumberFormattingPipe implements PipeTransform {
  constructor(private decimalPipe: DecimalPipe) {}

  transform(value: number | number[], decimalFormat?: string): unknown {
    if (Array.isArray(value)) {
      return value.map(num => this.decimalPipe.transform(num, decimalFormat));
    }
    return this.decimalPipe.transform(value, decimalFormat);
  }

}
