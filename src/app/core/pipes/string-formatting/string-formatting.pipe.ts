import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringFormatting'
})
export class StringFormattingPipe implements PipeTransform {

  transform(strings: string[] | string[][]): string {
    return this.transformStringOrStrings(
      strings.map(string => this.transformStringOrStrings(string))
    );
  }

  private transformStringOrStrings(stringOrStrings: string | string[]): string {
    if (typeof stringOrStrings === 'string') {
      return stringOrStrings;
    }
    if (stringOrStrings.length === 1) {
      return stringOrStrings[0];
    }
    return `[ ${stringOrStrings.join(', ')} ]`;
  }
}
