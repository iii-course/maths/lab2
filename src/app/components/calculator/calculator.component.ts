import {ChangeDetectorRef, Component, Input, OnChanges, OnInit} from '@angular/core';
import {CalculatorService} from "../../core/services/calculator/calculator.service";
import {Iteration, Output} from "../../core/models/output.model";
import {AlgorithmParams, InputValues} from "../../core/models/input.model";
import {Observable} from "rxjs";

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit, OnChanges {
  @Input() inputValues!: InputValues;
  @Input() algorithmParams!: AlgorithmParams;

  public iterations$!: Observable<Iteration[]>;
  public result!: Output;
  public error!: Error;

  public displayedColumns = ['numberOfIteration', 'stage', 'populationReal', 'populationBinary'];

  constructor(private calculator: CalculatorService) { }

  public ngOnInit(): void {
    this.iterations$ = this.calculator.iterations$;
  }

  public ngOnChanges(): void {
    if (this.inputValues && this.algorithmParams) {
      this.calculate();//this.iterations = this.calculator.iterations;
    }
  }

  private calculate(): void {
    const result = this.calculator.calculate(this.inputValues, this.algorithmParams);
    if (result instanceof Error) {
      this.error = result;
    } else {
      this.result = result;
    }
  }
}
