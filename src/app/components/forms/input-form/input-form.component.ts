import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {InputValues} from "../../../core/models/input.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Interval} from "../../../core/models/vectors/interval.model";
import {functions} from "../../../core/data/functions";
import {FunctionData} from "../../../core/models/function-data";

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit {
  @Output() inputSubmitted = new EventEmitter<InputValues>();

  public formGroup!: FormGroup;
  public functions!: FunctionData[];

  constructor(private fb: FormBuilder) { }

  public ngOnInit(): void {
    this.functions = functions;
    this.formGroup = this.fb.group({
      intervalBegin: [0, [Validators.min(0), Validators.max(1000000)]],
      intervalEnd: [10,  [Validators.min(0), Validators.max(1000000)]],
      populationSize: [undefined, [Validators.required, Validators.min(1), Validators.max(1000)]],
      maxNumberOfIterations: [undefined, Validators.required],
      precision: [0.01, [Validators.required, Validators.min(0.000001), Validators.max(10)]],
      mutationProbability: [1, [Validators.required, Validators.min(0), Validators.max(1)]],
      functionData: [undefined, Validators.required]
    });
  }

  public onSubmit() {
    const input = this.getFormattedFormValue();
    this.inputSubmitted.emit(input);
  }

  private getFormattedFormValue(): InputValues {
    const input = new Map();
    const {value} = this.formGroup;
    const interval = this.getIntervalFromForm();
    if (interval) {
      input.set('interval', interval);
    }
    Object.entries(value)
      .filter(([_, value]) => value)
      .forEach(([key, val]) => input.set(key, val));
    return Object.fromEntries(input);
  }

  private getIntervalFromForm(): Interval | undefined {
    const {intervalBegin, intervalEnd} = this.formGroup.value;
    if ((typeof intervalBegin === 'number') && (typeof intervalEnd === 'number')) {
      return new Interval({
        begin: intervalBegin,
        end: intervalEnd
      });
    }
    return undefined;
  }
}
