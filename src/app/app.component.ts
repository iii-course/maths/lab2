import { Component } from '@angular/core';
import {AlgorithmParams, InputValues} from "./core/models/input.model";
import {RandomPopulationGenerator} from "./core/models/population-generators/random-population.generator";
import {RouletteWheelSelector} from "./core/models/breeding-selectors/roulette-wheel.selector";
import {DoublePointCrossoverBreeder} from "./core/models/crossover-breeders/double-point.crossover-breeder";
import {MultiPointMutator} from "./core/models/mutators/multi-point.mutator";
import {BolzmanSelector} from "./core/models/selectors/bolzman.selector";
import {EliteSelector} from "./core/models/selectors/elite.selector";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public inputValues!: InputValues;
  public algorithmParams!: AlgorithmParams;

  constructor() {
    this.algorithmParams = {
      populationGenerator: new RandomPopulationGenerator(),
      breedingSelector: new RouletteWheelSelector(),
      crossoverBreeder: new DoublePointCrossoverBreeder(),
      mutator: new MultiPointMutator(),
      selector: new EliteSelector()
    }
  }

  public calculate(input: InputValues) {
    this.inputValues = input;
  }
}
